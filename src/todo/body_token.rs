// TODO: support arbitrary kv in todos e.g., foo:xyz?

// TODO: dependent tasks, nanoids //
// https://cdn.rawgit.com/bram85/topydo/master/docs/index.html#Dependencies
// #[derive(Eq, PartialEq, Debug)]
// enum ID {
//     // these are nano IDs
//     ID(String),
// }

use crate::TODO_DATE_FORMAT;

#[derive(Eq, PartialEq, Debug, Clone)]
pub enum TodoBodyToken {
    Word(String),
    Context(String),
    Project(String),
    ThresholdDate(time::Date),
    DueDate(time::Date),
    Hidden(bool),
}

impl std::fmt::Display for TodoBodyToken {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use TodoBodyToken::*;
        match self {
            Word(s) => write!(f, "{}", s),
            Context(s) => write!(f, "{}", s),
            Project(s) => write!(f, "{}", s),
            ThresholdDate(d) => write!(f, "t:{}", d.format(TODO_DATE_FORMAT).unwrap()),
            DueDate(d) => write!(f, "due:{}", d.format(TODO_DATE_FORMAT).unwrap()),
            Hidden(true) => write!(f, "h:1"),
            Hidden(false) => write!(f, "h:0"),
        }
    }
}
