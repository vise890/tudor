use colored::{ColoredString, Colorize};

use crate::todo::body_token::TodoBodyToken;
use crate::todo::Todo;
use crate::TODO_DATE_FORMAT;

impl From<TodoBodyToken> for ColoredString {
    fn from(tbt: TodoBodyToken) -> ColoredString {
        use TodoBodyToken::*;
        match tbt {
            Word(s) => s.normal(),
            Context(s) => s.magenta(),
            Project(s) => s.red(),
            ThresholdDate(d) => format!("t:{}", d.format(TODO_DATE_FORMAT).unwrap()).green(),
            DueDate(d) => format!("due:{}", d.format(TODO_DATE_FORMAT).unwrap()).green(),
            Hidden(true) => "h:1".dimmed(),
            Hidden(false) => "h:0".dimmed(),
        }
    }
}

pub struct TermTodo {
    s: String,
}

impl From<Todo> for TermTodo {
    fn from(todo: Todo) -> TermTodo {
        let mut tokens = Vec::new();
        if todo.is_completed {
            tokens.push("x".normal())
        };
        if let Some(p) = todo.priority {
            let chr = p.to_uppercase().next().unwrap();
            let pri = format!("({})", chr);
            let pri = match chr {
                'A' => pri.red(),
                'B' => pri.yellow(),
                'C' => pri.green(),
                'D' => pri.blue(),
                _ => pri.normal(),
            };
            tokens.push(pri)
        };
        for tbt in todo.body_tokens {
            tokens.push(tbt.into())
        }
        let tokens: Vec<String> = tokens.iter().map(ToString::to_string).collect();
        let mut s = tokens.join(" ");
        if todo.is_completed {
            s = s.dimmed().strikethrough().to_string()
        }
        TermTodo { s }
    }
}

impl std::fmt::Display for TermTodo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.s)
    }
}
