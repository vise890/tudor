use std::fs::File;
use std::io;
use std::path::PathBuf;

use anyhow::{Context, Result};
use structopt::StructOpt;

use tudor::config::Config;
use tudor::sql::expression::Expression;
use tudor::todo::term_todo::TermTodo;
use tudor::todo::Todo;

const APP_NAME: &str = "tudor";
const DEFAULT_TODO_TXT_FILE_PATH: &str = "todo.txt";
// const DEFAULT_CONFIG_FILE_PATH: &str = "tudor.toml";

/// Filters todo.txt files with a SQL-y dialect
#[derive(StructOpt, Debug)]
#[structopt(
name = APP_NAME,
version = "0.0.1",
author = "Martino Visintin <martino@visint.in>"
)]
struct Opt {
    /// Prints raw todos (no formatting, dates are shown, etc.)
    #[structopt(short, long)]
    raw: bool,

    /// The path of the input todo.txt file (or done.txt/anything.txt) to use
    #[structopt(short = "f", long = "file", default_value = DEFAULT_TODO_TXT_FILE_PATH, parse(from_os_str))]
    todo_txt_file_path: PathBuf,

    /// The expression to filter todos
    #[structopt(short, long)]
    expression: Option<String>,

    /// The path of the config file to use
    #[structopt(short = "c", long = "config", parse(from_os_str))]
    config_file_path: Option<PathBuf>,

    /// The view from config to filter todos
    #[structopt(name = "VIEW")]
    view_name: Option<String>,
}

fn main() -> Result<()> {
    let opt = Opt::from_args();
    let config =
        get_config(&opt.config_file_path).with_context(|| "Could not get valid config")?;
    let expr = match (&opt.expression, &opt.view_name) {
        (None, None) => config.get_default_expression(),
        (None, Some(view_name)) => config.get_expression(&view_name).with_context(|| {
            format!(
                "Could not get view {}. Available views are {:?}",
                view_name,
                config.list_views()
            )
        })?,
        (Some(expr_str), _) => {
            unimplemented!("custom expressions {} ", expr_str)
            // &Expression::parse(&expr_str)
            //     .with_context(|| format!("Provided expression '{}' failed to parse", expr_str))?
        }
    };
    let todo_file = File::open(&opt.todo_txt_file_path)
        .with_context(|| format!("Could not read todo file {:#?}", &opt.todo_txt_file_path))?;
    let todo_txt = io::BufReader::new(todo_file);
    let stdout = io::stdout();
    let writer = io::BufWriter::new(stdout.lock());
    print_filtered_todos(opt.raw, &expr, Box::new(todo_txt), writer);
    Ok(())
}

fn get_config(config_file_path: &Option<PathBuf>) -> Result<Config> {
    match config_file_path {
        None => {
            let local_config_path = std::path::Path::new("tudor.toml");
            if local_config_path.exists() {
                Config::read_from_file(local_config_path)
            } else {
                Ok(Config::read_default())
            }
        }
        Some(config_file_path) => {
            let config_exists = std::path::Path::new(&config_file_path).exists();
            if config_exists {
                Config::read_from_file(config_file_path)
            } else {
                // TODO: return anyhow error?
                eprintln!(
                    "Provided config file '{:?}' failed to parse:",
                    config_file_path
                );
                std::process::exit(exitcode::NOINPUT);
            }
        }
    }
}

fn print_filtered_todos<W: io::Write, R: io::BufRead>(
    raw: bool,
    expr: &Expression,
    input: R,
    mut output: W,
) {
    for (idx, line) in input.lines().enumerate() {
        let l = line.unwrap();
        if !l.is_empty() {
            let t = Todo::parse(&l).unwrap();
            if expr.eval(&t).is_true() {
                if raw {
                    writeln!(output, "{}", t).unwrap(); // TODO: err handling
                } else {
                    let tt: TermTodo = t.into();
                    writeln!(output, "|{:2}| {}", idx + 1, tt).unwrap(); // TODO: err handling
                }
            }
        }
    }
}
