#![feature(test)]

#[macro_use]
extern crate lazy_static;
extern crate test;

use time::format_description::FormatItem;
use time::macros::format_description;

pub mod sql;
pub mod todo;

pub mod config;
pub mod utils;

const TODO_DATE_FORMAT: &'static [FormatItem<'static>] = format_description!("[year]-[month]-[day]");
