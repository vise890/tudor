use std::fmt::Debug;
use std::path::Path;

use anyhow::{Context, Result};
use serde_derive::Deserialize;

use crate::config::raw_config::GetExpressionError::NoSuchViewError;
use crate::sql::expression::Expression;

#[derive(Deserialize, Debug)]
pub struct RawConfig {
    pub views: Vec<RawView>,
}

#[derive(Deserialize, Debug)]
pub struct RawView {
    pub name: String,
    pub icon: Option<String>,
    pub expr: String,
}

const DEFAULT_CONFIG_STR: &str = include_str!("default.toml");

lazy_static! {
    static ref DEFAULT_RAW_CONFIG: RawConfig = RawConfig::read_default();
}

impl RawConfig {
    pub fn read_default() -> Self {
        toml::from_str(DEFAULT_CONFIG_STR).unwrap() // default config should always parse
    }

    pub fn default() -> &'static Self {
        &DEFAULT_RAW_CONFIG
    }

    pub fn read_from_file<P>(config_path: P) -> Result<RawConfig>
        where
            P: AsRef<Path> + Debug,
    {
        let str = std::fs::read_to_string(&config_path)
            .with_context(|| format!("could not read config file at {:?}", &config_path))?;
        let config = toml::from_str(&str)
            .with_context(|| format!("could not parse config file {:?} into toml", &config_path))?;
        Ok(config)
    }

    pub fn list_views(&self) -> Vec<&String> {
        self.views.iter().map(|v| &v.name).collect()
    }

    pub fn get_expression(&self, view_name: &str) -> Result<Expression, GetExpressionError> {
        match self.views.iter().find(|rv| rv.name == view_name) {
            Some(raw_view) => Ok(Expression::parse(&raw_view.expr)?),
            None => Err(NoSuchViewError)
        }
    }
}

#[derive(Debug)]
pub enum GetExpressionError {
    NoSuchViewError,
    ExpressionParseError(crate::sql::expression::ParseError),
}

impl From<crate::sql::expression::ParseError> for GetExpressionError {
    fn from(e: crate::sql::expression::ParseError) -> Self {
        GetExpressionError::ExpressionParseError(e)
    }
}

#[cfg(test)]
mod test {
    use ::test::Bencher;

    use super::*;

    #[bench]
    fn reading_default_raw_config_bench(b: &mut Bencher) {
        b.iter(|| RawConfig::read_default());
    }

    #[bench]
    fn getting_lazy_static_default_raw_config_bench_should_be_0ish(b: &mut Bencher) {
        b.iter(|| RawConfig::default());
    }

    #[bench]
    fn getting_expression_from_default_raw_config(b: &mut Bencher) {
        b.iter(|| RawConfig::default().get_expression("active"));
    }

    #[test]
    fn raw_config_can_be_parsed_from_inbuilt_str() {
        let rc = RawConfig::read_default();
        assert!(!rc.list_views().is_empty())
    }
}
