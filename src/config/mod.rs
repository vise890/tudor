use std::collections::HashMap;
use std::convert::TryFrom;
use std::fmt::Debug;
use std::path::Path;

use anyhow::{Context, Result};

use crate::config::raw_config::RawConfig;
use crate::config::raw_config::RawView;
use crate::sql::expression::{Expression, ParseError};

mod raw_config;

#[derive(Debug)]
pub struct Config {
    views: HashMap<String, View>,
}

#[derive(Debug)]
struct View {
    name: String,
    #[allow(dead_code)]
    icon: Option<String>,
    expr: Expression,
}

const DEFAULT_VIEW_NAME: &str = "active";

lazy_static! {
    static ref DEFAULT_CONFIG: Config = Config::read_default();
}

#[allow(dead_code)]
impl Config {
    pub fn read_default() -> Self {
        Config::try_from(RawConfig::default()).unwrap() // expressions in default config should always parse
    }

    pub fn read_from_file<P>(config_path: P) -> Result<Config>
        where
            P: AsRef<Path> + Debug,
    {
        let raw_config = RawConfig::read_from_file(&config_path)?;
        let config = Config::try_from(&raw_config)
            .with_context(|| format!("could not parse views in '{:#?}'", &config_path))?;
        Ok(config)
    }

    pub fn default() -> &'static Self {
        &DEFAULT_CONFIG
    }

    pub fn get_expression(&self, view_name: &str) -> Option<&Expression> {
        let view = self.views.get(view_name)?;
        Some(&view.expr)
    }

    pub fn get_default_expression(&self) -> &Expression {
        &self.views.get(DEFAULT_VIEW_NAME).unwrap().expr
    }

    pub fn list_views(&self) -> Vec<&String> {
        self.views.keys().collect()
    }
}

impl TryFrom<&RawConfig> for Config {
    type Error = anyhow::Error;
    fn try_from(rc: &RawConfig) -> Result<Self> {
        let mut views = HashMap::with_capacity(rc.views.len());
        for raw_view in rc.views.iter() {
            let pv = View::try_from(raw_view)
                .with_context(|| format!("could not parse view {}", raw_view.name))?;
            views.insert(pv.name.to_string(), pv);
        }
        Ok(Config { views })
    }
}

impl TryFrom<&RawView> for View {
    type Error = ParseError;
    fn try_from(rv: &RawView) -> Result<Self, ParseError> {
        Ok(View {
            name: rv.name.clone(),
            icon: rv.icon.clone(),
            expr: Expression::parse(&rv.expr)?,
        })
    }
}

#[cfg(test)]
mod test {
    use ::test::Bencher;

    use crate::todo::Todo;

    use super::*;

    #[bench]
    fn reading_default_config_bench(b: &mut Bencher) {
        b.iter(|| Config::read_default());
    }

    #[bench]
    fn getting_expression_from_default_config_should_be_0ish(b: &mut Bencher) {
        b.iter(|| Config::default().get_expression("active"));
    }

    #[test]
    fn default_config_parses() {
        let c = &DEFAULT_CONFIG;

        let inbox_expr = &c.views.get("inbox").unwrap().expr;
        let t = Todo::parse("I haz no context").unwrap();
        assert!(inbox_expr.eval(&t).is_true());
    }

    #[test]
    fn convert_raw_config_into_parsed_config() {
        let pc = Config::try_from(RawConfig::default()).unwrap();
        assert!(!pc.list_views().is_empty())
    }
}
