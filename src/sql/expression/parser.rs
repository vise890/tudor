use std::collections::BTreeSet;

use pest::prec_climber::PrecClimber;
use pest_consume::{match_nodes, Parser};

use crate::sql::expression::Expression;
use crate::sql::expression::function::Function;
use crate::sql::expression::operator::comparison::ComparisonOperator;
use crate::sql::expression::todo_field::TodoField;
use crate::sql::expression::value::Value;

#[derive(Parser)]
#[grammar = "sql/todosql.pest"]
pub struct ExpressionParser;

pub type Error = pest_consume::Error<Rule>;
pub type Result<T> = std::result::Result<T, Error>;
type Node<'i> = pest_consume::Node<'i, Rule, ()>;

lazy_static! {
    // loosely based on:
    // https://www.postgresql.org/docs/current/sql-syntax-lexical.html#SQL-PRECEDENCE
    static ref PREC_CLIMBER: PrecClimber<Rule> = {
        use pest::prec_climber::Assoc::Left;
        use pest::prec_climber::Operator;
        use Rule::*;
        PrecClimber::new(vec![
            Operator::new(or, Left),
            Operator::new(and, Left),
            Operator::new(is, Left) | Operator::new(is_not, Left),
            Operator::new(comparison_operator, Left),
            Operator::new(in_, Left) | Operator::new(not_in, Left),
        ])
    };
}

#[pest_consume::parser]
impl ExpressionParser {
    fn EOI(_n: Node) -> Result<()> {
        Ok(())
    }

    /// ## Value
    fn null(_n: Node) -> Result<Value> {
        Ok(Value::Null)
    }
    fn boolean(n: Node) -> Result<Value> {
        let v = match n.as_str() {
            "true" => Value::Boolean(true),
            "false" => Value::Boolean(false),
            _ => unreachable!(),
        };
        Ok(v)
    }
    fn text(n: Node) -> Result<Value> {
        let v = Value::Text(n.as_str().to_string().replace("'", ""));
        Ok(v)
    }
    fn empty_set_of_texts(n: Node) -> Result<Value> {
        Ok(Value::Set(BTreeSet::new()))
    }
    fn non_empty_set_of_texts(n: Node) -> Result<Value> {
        let mut set = BTreeSet::new();
        match_nodes!(n.into_children();
           [text(ts)..] => {
               for t in ts {
                    match t {
                        Value::Text(s)  => set.insert(s),
                        _ => unreachable!(),
                    };
               };
           }
        );
        Ok(Value::Set(set))
    }
    fn set_of_texts(n: Node) -> Result<Value> {
        Ok(match_nodes!(n.into_children();
           [empty_set_of_texts(s)] =>  s,
           [non_empty_set_of_texts(s)] =>  s,
        ))
    }
    pub fn value(n: Node) -> Result<Value> {
        Ok(match_nodes!(n.into_children();
            [null(v)] => v,
            [boolean(v)] => v,
            [text(v)] => v,
            [set_of_texts(v)] => v,
        ))
    }

    /// ## Negated
    pub fn negated(n: Node) -> Result<Expression> {
        Ok(match_nodes!(n.into_children();
            [expression(e)] => {
                Expression::Negated(Box::new(e))
            }
        ))
    }

    /// ## Comparison Operator
    fn gt(_n: Node) -> Result<ComparisonOperator> {
        Ok(ComparisonOperator::Gt)
    }
    fn lt(_n: Node) -> Result<ComparisonOperator> {
        Ok(ComparisonOperator::Lt)
    }
    fn gt_eq(_n: Node) -> Result<ComparisonOperator> {
        Ok(ComparisonOperator::GtEq)
    }
    fn lt_eq(_n: Node) -> Result<ComparisonOperator> {
        Ok(ComparisonOperator::LtEq)
    }
    fn eq(_n: Node) -> Result<ComparisonOperator> {
        Ok(ComparisonOperator::Eq)
    }
    fn not_eq(_n: Node) -> Result<ComparisonOperator> {
        Ok(ComparisonOperator::NotEq)
    }
    fn contains(_n: Node) -> Result<ComparisonOperator> {
        Ok(ComparisonOperator::Contains)
    }
    fn is_contained_by(_n: Node) -> Result<ComparisonOperator> {
        Ok(ComparisonOperator::IsContainedBy)
    }
    fn overlap(_n: Node) -> Result<ComparisonOperator> {
        Ok(ComparisonOperator::Overlap)
    }
    pub fn comparison_operator(n: Node) -> Result<ComparisonOperator> {
        Ok(match_nodes!(n.into_children();
            [gt(bo)] => bo,
            [lt(bo)] => bo,
            [gt_eq(bo)] => bo,
            [lt_eq(bo)] => bo,
            [eq(bo)] => bo,
            [not_eq(bo)] => bo,
            [contains(bo)] => bo,
            [is_contained_by(bo)] => bo,
            [overlap(bo)] => bo,
        ))
    }

    /// ## Function
    fn function_name(n: Node) -> Result<String> {
        Ok(n.as_str().to_string())
    }
    fn function_argument_list(n: Node) -> Result<Vec<Expression>> {
        Ok(match_nodes!(n.into_children();
            [expression(es)..] => es.collect()
        ))
    }
    pub fn function(n: Node) -> Result<Function> {
        Ok(match_nodes!(n.into_children();
            [function_name(name), function_argument_list(args)] => {
                Function { name, args }
            },
        ))
    }

    /// ## Todo Field
    fn full_text(_n: Node) -> Result<TodoField> {
        Ok(TodoField::FullText)
    }
    fn is_completed(_n: Node) -> Result<TodoField> {
        Ok(TodoField::IsCompleted)
    }
    fn id(_n: Node) -> Result<TodoField> {
        Ok(TodoField::Id)
    }
    fn is_blocking_for(_n: Node) -> Result<TodoField> {
        Ok(TodoField::IsBlockingFor)
    }
    fn is_blocked(_n: Node) -> Result<TodoField> {
        Ok(TodoField::IsBlocked)
    }
    fn blocked_by(_n: Node) -> Result<TodoField> {
        Ok(TodoField::BlockedBy)
    }
    fn creation_date(_n: Node) -> Result<TodoField> {
        Ok(TodoField::CreationDate)
    }
    fn completion_date(_n: Node) -> Result<TodoField> {
        Ok(TodoField::CompletionDate)
    }
    fn priority(_n: Node) -> Result<TodoField> {
        Ok(TodoField::Priority)
    }
    fn canonical_context(_n: Node) -> Result<TodoField> {
        Ok(TodoField::CanonicalContext)
    }
    fn contexts(_n: Node) -> Result<TodoField> {
        Ok(TodoField::Contexts)
    }
    fn canonical_project(_n: Node) -> Result<TodoField> {
        Ok(TodoField::CanonicalProject)
    }
    fn projects(_n: Node) -> Result<TodoField> {
        Ok(TodoField::Projects)
    }
    fn due_date(_n: Node) -> Result<TodoField> {
        Ok(TodoField::DueDate)
    }
    fn threshold_date(_n: Node) -> Result<TodoField> {
        Ok(TodoField::ThresholdDate)
    }
    fn is_hidden(_n: Node) -> Result<TodoField> {
        Ok(TodoField::IsHidden)
    }
    pub fn todo_field(n: Node) -> Result<TodoField> {
        Ok(match_nodes!(n.into_children();
            [full_text(tf)] => tf,
            [is_completed(tf)] => tf,
            [id(tf)] => tf,
            [is_blocking_for(tf)] => tf,
            [is_blocked(tf)] => tf,
            [blocked_by(tf)] => tf,
            [creation_date(tf)] => tf,
            [completion_date(tf)] => tf,
            [priority(tf)] => tf,
            [canonical_context(tf)] => tf,
            [contexts(tf)] => tf,
            [canonical_project(tf)] => tf,
            [projects(tf)] => tf,
            [due_date(tf)] => tf,
            [threshold_date(tf)] => tf,
            [is_hidden(tf)] => tf,
        ))
    }

    /// ## Expression
    fn term(n: Node) -> Result<Expression> {
        Ok(match_nodes!(n.into_children();
            [todo_field(e)] => Expression::Identifier(e),
            [value(e)] => Expression::Value(e),
            [negated(e)] => e,
            [function(e)] => Expression::Function(e),
            [expression(e)] => e,
        ))
    }
    #[prec_climb(term, PREC_CLIMBER)]
    pub fn expression(l: Expression, op: Node, r: Expression) -> Result<Expression> {
        Ok(match op.as_rule() {
            Rule::in_ => Expression::In {
                negated: false,
                needle: Box::new(l),
                haystack: Box::new(r),
            },
            Rule::not_in => Expression::In {
                negated: true,
                needle: Box::new(l),
                haystack: Box::new(r),
            },
            Rule::comparison_operator => Expression::ComparisonOp {
                left: Box::new(l),
                op: ExpressionParser::comparison_operator(op)?,
                right: Box::new(r),
            },
            Rule::is_not => Expression::Is {
                negated: true,
                left: Box::new(l),
                right: Box::new(r),
            },
            Rule::is => Expression::Is {
                negated: false,
                left: Box::new(l),
                right: Box::new(r),
            },
            Rule::and => Expression::And {
                left: Box::new(l),
                right: Box::new(r),
            },
            Rule::or => Expression::Or {
                left: Box::new(l),
                right: Box::new(r),
            },
            r => return Err(op.error(format!("Rule {:?} isn't an operator", r))),
        })
    }

    /// Full input parse of an EXPRESSION only i.e. { SOI ~ expression ~ EOI }
    pub fn expression_seoi(n: Node) -> Result<Expression> {
        Ok(match_nodes!(n.into_children();
            [expression(e), EOI(_)] => e
        ))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parsing_todo_fields() {
        assert!(TodoField::parse("full_text").is_ok());
        assert!(TodoField::parse("is_completed").is_ok());
        assert!(TodoField::parse("is_blocked").is_ok());
        assert!(TodoField::parse("viscosity").is_err());
    }

    #[test]
    fn parsing_values() {
        assert_eq!(Value::parse("null"), Ok(Value::Null));
        assert_eq!(Value::parse("true"), Ok(Value::Boolean(true)));
        assert_eq!(Value::parse("false"), Ok(Value::Boolean(false)));

        assert_eq!(Value::parse("'foo'"), Ok(Value::Text("foo".to_string())));
    }

    #[test]
    fn parsing_sets() {
        let expected = Value::set(&vec!["a", "b", "z"]);

        assert_eq!(Value::parse("('a', 'b', 'z')"), Ok(expected.clone()));
        // with space
        assert_eq!(
            Value::parse("(  'a' ,   'b' , 'z'  )"),
            Ok(expected.clone())
        );
        // with trailing comma
        assert_eq!(Value::parse("('a', 'b', 'z',)"), Ok(expected)); // 🏴‍☠️
    }

    #[test]
    fn disambiguating_sets_and_nested_expressions() {
        // singleton set can be created with trailing comma
        assert_eq!(
            Expression::parse("('a',)"),
            Ok(Expression::Value(Value::set(&vec!["a"])))
        );
        assert_eq!(
            Expression::parse("('a')"),
            Ok(Expression::Value(Value::text("a")))
        );

        // empty set can be created with comma
        assert_eq!(Value::parse("(,)"), Ok(Value::set(&vec![])));
    }

    #[test]
    fn parsing_functions() {
        assert_eq!(
            Function::parse("noargs()"),
            Ok(Function {
                name: "noargs".to_string(),
                args: vec![],
            })
        );
        assert_eq!(
            Function::parse("day('2020-10-10')"),
            Ok(Function {
                name: "day".to_string(),
                args: vec![Expression::Value(Value::Text("2020-10-10".to_string()))],
            })
        );
        assert_eq!(
            Function::parse("foo(true, null,  'bar')"),
            Ok(Function {
                name: "foo".to_string(),
                args: vec![
                    Expression::Value(Value::Boolean(true)),
                    Expression::Value(Value::Null),
                    Expression::Value(Value::Text("bar".to_string())),
                ],
            })
        );
        assert_eq!(
            Function::parse("foo(ccontext, null,  'bar')"),
            Ok(Function {
                name: "foo".to_string(),
                args: vec![
                    Expression::Identifier(TodoField::CanonicalContext),
                    Expression::Value(Value::Null),
                    Expression::Value(Value::Text("bar".to_string())),
                ],
            })
        );
        assert_eq!(
            Function::parse("is_empty(contexts)"),
            Ok(Function {
                name: "is_empty".to_string(),
                args: vec![Expression::Identifier(TodoField::Contexts)],
            })
        );

        // FIXME: args are sets
        // assert_eq!(
        //     // Function::parse("foo(  ( 'a'  , 'b' ) , ('c' , 'd', ) )"),
        //     Ok(Function {
        //         name: "foo".to_string(),
        //         args: vec![
        //             Expression::Value(Value::set(&vec!["a", "b"])),
        //             Expression::Value(Value::set(&vec!["c", "d"])),
        //         ],
        //     })
        // );
    }

    #[test]
    fn parsing_comparison_operator() {
        assert_eq!(
            ComparisonOperator::parse("<>"),
            Ok(ComparisonOperator::NotEq)
        );
        assert_eq!(
            ComparisonOperator::parse("!="),
            Ok(ComparisonOperator::NotEq)
        );
        assert_eq!(ComparisonOperator::parse(">"), Ok(ComparisonOperator::Gt));
        assert_eq!(ComparisonOperator::parse("<"), Ok(ComparisonOperator::Lt));
        assert_eq!(
            ComparisonOperator::parse("<="),
            Ok(ComparisonOperator::LtEq)
        );
        assert_eq!(
            ComparisonOperator::parse(">="),
            Ok(ComparisonOperator::GtEq)
        );
        assert_eq!(
            ComparisonOperator::parse("@>"),
            Ok(ComparisonOperator::Contains)
        );
        assert_eq!(
            ComparisonOperator::parse("<@"),
            Ok(ComparisonOperator::IsContainedBy)
        );
        assert_eq!(
            ComparisonOperator::parse("&&"),
            Ok(ComparisonOperator::Overlap)
        );
        assert!(ComparisonOperator::parse("~~>").is_err());
        // NOTE: these still parse as `<` `>`...
        //       but the rest of the grammar should make it not happen, right??
        // assert!(ComparisonOperator::parse("<~~").is_err());
        // assert!(ComparisonOperator::parse("<$>").is_err());
        // assert!(ComparisonOperator::parse(">>=").is_err());
        // assert!(ComparisonOperator::parse("<~>").is_err());
    }

    #[test]
    fn parsing_term() {
        assert_eq!(
            Expression::parse("threshold_date").unwrap(),
            Expression::Identifier(TodoField::ThresholdDate)
        );
        assert_eq!(
            Expression::parse("(threshold_date)").unwrap(),
            Expression::Identifier(TodoField::ThresholdDate),
        );
        assert_eq!(
            Expression::parse("'abc'").unwrap(),
            Expression::Value(Value::Text("abc".to_string()))
        );
        assert_eq!(
            Expression::parse("null").unwrap(),
            Expression::Value(Value::Null)
        );
    }

    #[test]
    fn parse_todo_field_aliases() {
        let aliases = vec![
            "is_complete",
            "is_done",
            "completed",
            "complete",
            "done",
            "blocking_for",
            "blocks",
            "blocked",
            "created_on",
            "completed_on",
            "pri",
            "ccontext",
            "ctx",
            "list",
            "cproject",
            "ctxs",
            "lists",
            "prj",
            "tag",
            "prjs",
            "tags",
            "due_on",
            "start_date",
            "hidden",
            "h",
        ];

        aliases
            .iter()
            .map(|t| Expression::parse(t))
            .zip(aliases.iter())
            .for_each(|(res, orig)| assert!(res.is_ok(), "{} wasn't parsed", orig))
    }

    #[test]
    fn parsing_other_expressions() {
        assert_eq!(
            Expression::parse("not true").unwrap(),
            Expression::Negated(Box::from(Expression::Value(Value::Boolean(true))))
        );

        let mut exp_set = BTreeSet::new();
        exp_set.insert("@home".to_string());
        exp_set.insert("@pc".to_string());
        assert_eq!(
            Expression::parse("ccontext not in ('@home', '@pc')").unwrap(),
            Expression::In {
                negated: true,
                needle: Box::new(Expression::Identifier(TodoField::CanonicalContext)),
                haystack: Box::new(Expression::Value(Value::Set(exp_set.clone()))),
            }
        );
        assert_eq!(
            Expression::parse("ccontext in ('@home', '@pc')").unwrap(),
            Expression::In {
                negated: false,
                needle: Box::new(Expression::Identifier(TodoField::CanonicalContext)),
                haystack: Box::new(Expression::Value(Value::Set(exp_set))),
            }
        );

        assert_eq!(
            Expression::parse("true is not null").unwrap(),
            Expression::Is {
                negated: true,
                left: Box::new(Expression::Value(Value::Boolean(true))),
                right: Box::new(Expression::Value(Value::Null)),
            }
        );

        assert_eq!(
            Expression::parse("null is null").unwrap(),
            Expression::Is {
                negated: false,
                left: Box::new(Expression::Value(Value::Null)),
                right: Box::new(Expression::Value(Value::Null)),
            }
        );

        Expression::parse("(true == true) and (false != true) and ('sucka' <> 'blyat') or true")
            .unwrap();
    }
}
