use pest_consume::Parser;
use time::OffsetDateTime;

use crate::sql::expression::Expression;
use crate::sql::expression::parser::{ExpressionParser, Result, Rule};
use crate::sql::expression::value::Value;
use crate::TODO_DATE_FORMAT;

#[derive(Eq, PartialEq, Debug)]
pub struct Function {
    pub name: String,
    pub args: Vec<Expression>,
}

fn today() -> time::Date {
    OffsetDateTime::now_local().unwrap().date()
}

fn date(args: &[Value]) -> Value {
    match args {
        [Value::Text(date_string)] => {
            match date_string.as_str() {
                "today" => Value::Date(today()),
                s => Value::Date(time::Date::parse(s, TODO_DATE_FORMAT).unwrap()),
            }
        }
        _ => unimplemented!("foo({:#?})", args),
    }
}

fn is_empty(args: &[Value]) -> Value {
    match args {
        [Value::Set(s)] => Value::Boolean(s.is_empty()),
        _ => panic!("is_empty called with {:#?}, was expecting 1 set", args),
    }
}

impl Function {
    // TODO: date('today', '+ 2 weeks')
    // TODO: count(field)
    // TODO: size(field)
    pub fn apply(&self, args: &[Value]) -> Value {
        match self.name.as_str() {
            "date" => date(args),
            "is_empty" => is_empty(args),
            _ => unimplemented!(),
        }
    }

    pub fn parse(input: &str) -> Result<Function> {
        let nodes = ExpressionParser::parse(Rule::function, input)?;
        let node = nodes.single()?;
        let f = ExpressionParser::function(node)?;
        Ok(f)
    }
}
