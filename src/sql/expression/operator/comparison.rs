use pest_consume::Parser;

use crate::sql::expression::parser::{ExpressionParser, Result, Rule};
use crate::sql::expression::value::Value;

#[derive(Eq, PartialEq, Debug)]
pub enum ComparisonOperator {
    /// `=`
    Eq,
    /// `<>`  or `!=`
    NotEq,
    /// `>`
    Gt,
    /// `<`
    Lt,
    /// `>=`
    GtEq,
    /// `<=`
    LtEq,
    /// `@>`
    ///
    /// https://www.postgresql.org/docs/9.0/functions-array.html
    Contains,
    /// `<@`
    ///
    /// https://www.postgresql.org/docs/9.0/functions-array.html
    IsContainedBy,
    /// `&&`
    ///
    /// https://www.postgresql.org/docs/9.0/functions-array.html
    Overlap,
}

impl ComparisonOperator {
    pub fn apply(&self, left: &Value, right: &Value) -> Value {
        use ComparisonOperator::*;
        use Value::*;
        match (left, self, right) {
            (Null, _, _) => Null,
            (_, _, Null) => Null,

            (left, Eq, right) => Boolean(left == right),
            (left, NotEq, right) => Boolean(left != right),
            (left, Lt, right) => Boolean(left < right),
            (left, LtEq, right) => Boolean(left <= right),
            (left, Gt, right) => Boolean(left > right),
            (left, GtEq, right) => Boolean(left >= right),

            (Set(left), Overlap, Set(right)) => Boolean(left.intersection(right).next().is_some()),
            (Set(left), Overlap, Text(right)) => Boolean(left.contains(right)),

            _ => unimplemented!("{:#?} {:#?} {:#?}", left, self, right),
        }
    }
    pub fn parse(input: &str) -> Result<ComparisonOperator> {
        let nodes = ExpressionParser::parse(Rule::comparison_operator, input)?;
        let node = nodes.single()?;
        let v = ExpressionParser::comparison_operator(node)?;
        Ok(v)
    }
}
