use std::str::FromStr;

use pest_consume::Parser;

use crate::sql::expression::function::Function;
use crate::sql::expression::operator::comparison::ComparisonOperator;
use crate::sql::expression::parser::{ExpressionParser, Rule};
use crate::sql::expression::todo_field::TodoField;
use crate::sql::expression::value::Value;
use crate::todo::Todo;

mod function;
mod operator;
mod parser;
pub mod todo_field;
mod value;

pub type ParseResult<T> = crate::sql::expression::parser::Result<T>;
pub type ParseError = crate::sql::expression::parser::Error;

/// adapted from https://github.com/ballista-compute/sqlparser-rs/blob/main/src/ast/mod.rs
#[derive(Eq, PartialEq, Debug)]
pub enum Expression {
    /// Identifier e.g. table name or column name
    Identifier(TodoField),
    /// A literal value, such as string, number, date or NULL
    Value(Value),
    /// Unary operation e.g. `NOT false`
    Negated(Box<Expression>),
    /// Scalar function call e.g. `date('today', '+ 5 days')`
    Function(Function),
    /// `<expr> [ NOT ] IN <expr>`
    In {
        negated: bool,
        needle: Box<Expression>,
        haystack: Box<Expression>,
    },
    /// `<expr> IS [NOT] <expr>`
    Is {
        negated: bool,
        left: Box<Expression>,
        right: Box<Expression>,
    },
    /// Comparison operation e.g. `1 = 1` or `foo > bar`
    ComparisonOp {
        left: Box<Expression>,
        op: ComparisonOperator,
        right: Box<Expression>,
    },
    /// `<expr> AND <expr>`
    And {
        left: Box<Expression>,
        right: Box<Expression>,
    },
    /// `<expr> OR <expr>`
    Or {
        left: Box<Expression>,
        right: Box<Expression>,
    },
    // TODO: implement Display
    // TODO: case?
}

impl Expression {
    pub fn parse(input: &str) -> ParseResult<Expression> {
        let nodes = ExpressionParser::parse(Rule::expression_seoi, input)?;
        let node = nodes.single()?;
        let e = ExpressionParser::expression_seoi(node)?;
        Ok(e)
    }

    pub fn eval(&self, t: &Todo) -> Value {
        match self {
            Expression::Identifier(todo_field) => todo_field.get_value(t),
            Expression::Value(v) => v.clone(),
            Expression::ComparisonOp { left, op, right } => op.apply(&left.eval(t), &right.eval(t)),
            Expression::Is {
                left,
                negated,
                right,
            } => {
                let left = left.eval(t);
                let right = right.eval(t);
                let is = match (&left, &right) {
                    (Value::Null, Value::Null) => true,
                    _ => left == right,
                };
                let b = if *negated { !is } else { is };
                Value::Boolean(b)
            }
            Expression::Function(f) => {
                let args: Vec<Value> = f.args.iter().map(|a| a.eval(t)).collect();
                f.apply(&args)
            }
            Expression::And { left, right } => {
                let left = left.eval(t);
                if left.is_null() {
                    Value::Null
                } else if left.is_true() {
                    right.eval(t)
                } else if left.is_false() {
                    left
                } else {
                    unreachable!("{:#?},\nleft={:#?},\n right={:#?}", self, left, right)
                }
            }
            Expression::Or { left, right } => {
                let left = left.eval(t);
                if left.is_true() {
                    left
                } else {
                    right.eval(t)
                }
            }
            Expression::In {
                needle,
                negated,
                haystack,
            } => {
                let needle = needle.eval(&t);
                let haystack = haystack.eval(&t);
                match (needle, haystack) {
                    (Value::Text(str), Value::Set(set)) => {
                        let is_in = set.contains(&str);
                        let b = if *negated { !is_in } else { is_in };
                        Value::Boolean(b)
                    }
                    (n, h) => unimplemented!("{:#?} [not] in {:#?}", n, h),
                }
            }
            Expression::Negated(expr) => {
                let val = expr.eval(t);
                match val {
                    Value::Null => Value::Null,
                    Value::Boolean(b) => Value::Boolean(!b),
                    _ => panic!("Type error in Expression::Negated. Inner expression {:#?} evaluated to {:#?}. Expected Value::Null or Value::Boolean", expr, val)
                }
            }
        }
    }
}

impl FromStr for Expression {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Expression::parse(s)
    }
}

#[cfg(test)]
mod test {
    use ::test::Bencher;

    use super::*;

    #[test]
    fn evaluating_is_expressions() {
        let t = Todo::parse("x (A) 2020-12-25 ma vai t:2020-12-30").unwrap();

        let e = Expression::parse("null is null").unwrap();
        assert_eq!(e.eval(&t), Value::Boolean(true));

        let e = Expression::parse("true is true").unwrap();
        assert_eq!(e.eval(&t), Value::Boolean(true));

        let e = Expression::parse("false is false").unwrap();
        assert_eq!(e.eval(&t), Value::Boolean(true));

        let e = Expression::parse("null is not null").unwrap();
        assert_eq!(e.eval(&t), Value::Boolean(false));

        let e = Expression::parse("null is not false").unwrap();
        assert_eq!(e.eval(&t), Value::Boolean(true));

        let e = Expression::parse("null is not true").unwrap();
        assert_eq!(e.eval(&t), Value::Boolean(true));
    }

    #[test]
    fn evaluating_expressions() {
        let t = Todo::parse("x (A) 2020-12-25 ma vai t:2020-12-30").unwrap();

        let e = Expression::parse("date('2020-12-30')").unwrap();
        assert_eq!(e.eval(&t), Value::Date(time::macros::date!(2020 - 12 - 30)));
    }

    #[test]
    fn evaluating_and_expressions() {
        let t = Todo::parse("x (A) 2020-12-25 ma vai t:2020-12-30").unwrap();

        let e = Expression::parse("null and true").unwrap();
        assert_eq!(e.eval(&t), Value::Null);
        let e = Expression::parse("true and null").unwrap();
        assert_eq!(e.eval(&t), Value::Null);
        let e = Expression::parse("false and null").unwrap();
        assert_eq!(e.eval(&t), Value::Boolean(false));
        let e = Expression::parse("true and ('a' == 'a')").unwrap();
        assert_eq!(e.eval(&t), Value::Boolean(true));
        let e = Expression::parse("true and null and true").unwrap();
        assert_eq!(e.eval(&t), Value::Null);
        let e = Expression::parse("true and true and true").unwrap();
        assert_eq!(e.eval(&t), Value::Boolean(true));
        let e = Expression::parse("true and true and false").unwrap();
        assert_eq!(e.eval(&t), Value::Boolean(false));
        let e = Expression::parse("true and true and null").unwrap();
        assert_eq!(e.eval(&t), Value::Null);
    }

    #[test]
    fn evaluating_or_expressions() {
        let t = Todo::parse("x (A) 2020-12-25 ma vai t:2020-12-30").unwrap();

        let e = Expression::parse("null or true").unwrap();
        assert_eq!(e.eval(&t), Value::Boolean(true));
        let e = Expression::parse("false or null").unwrap();
        assert_eq!(e.eval(&t), Value::Null);
        let e = Expression::parse("false or true").unwrap();
        assert_eq!(e.eval(&t), Value::Boolean(true));
        let e = Expression::parse("false or false or true").unwrap();
        assert_eq!(e.eval(&t), Value::Boolean(true));
        let e = Expression::parse("false or false or true").unwrap();
        assert_eq!(e.eval(&t), Value::Boolean(true));
        let e = Expression::parse("false or false").unwrap();
        assert_eq!(e.eval(&t), Value::Boolean(false));
        let e = Expression::parse("false or ('a' == 'a')").unwrap();
        assert_eq!(e.eval(&t), Value::Boolean(true));
    }

    fn assert_todo_matches(todo: &str, expr: &str, expected: bool) {
        let expr = Expression::parse(expr).unwrap();
        let todo = Todo::parse(todo).unwrap();
        assert_eq!(expr.eval(&todo), Value::Boolean(expected))
    }

    #[test]
    fn matching_todos_with_expressions() {
        assert_todo_matches(
            "x (A) 2020-12-25 ma vai t:2020-12-30",
            "priority = 'A'",
            true,
        );
        assert_todo_matches(
            "x (A) 2020-12-25 ma vai t:2020-12-30",
            "priority = 'B'",
            false,
        );
        assert_todo_matches("x (A) 2020-12-25 ma vai t:2020-12-30", "is_completed", true);
        assert_todo_matches(
            "x (A) 2020-12-25 ma vai t:2020-12-30",
            "completion_date is null",
            true,
        );
        assert_todo_matches(
            "x (A) 2020-12-25 ma vai t:2020-12-30",
            "creation_date = date('2020-12-25')",
            true,
        );
        assert_todo_matches(
            "x (A) 2020-12-25 ma vai t:2020-12-30",
            "creation_date = date('2020-12-23')",
            false,
        );
        assert_todo_matches(
            "x (A) 2020-12-25 ma vai t:2020-12-30",
            "threshold_date = date('2020-12-30')",
            true,
        );
        assert_todo_matches(
            "x (A) 2020-12-25 ma vai t:2020-12-30",
            "is_hidden = false",
            true,
        );
    }

    #[test]
    fn matching_todos_with_and_search_clauses() {
        assert_todo_matches(
            "x (A) 2020-12-25 ma vai t:2020-12-30",
            "is_hidden = false and is_completed = true",
            true,
        );

        assert_todo_matches(
            "x (A) 2020-12-25 ma vai t:2020-12-30",
            "is_hidden = false and is_completed = false",
            false,
        );
    }

    #[test]
    fn matching_todos_with_or_search_clauses() {
        assert_todo_matches(
            "x (A) 2020-12-25 ma vai t:2020-12-30",
            "is_completed = false or creation_date = date('2020-12-25')",
            true,
        );

        assert_todo_matches(
            "x (A) 2020-12-25 ma vai t:2020-12-30",
            "is_hidden = true or is_completed = false",
            false,
        );
    }

    /// naive cases from:
    /// https://vapor.visint.in/1_rings/tudor/3-todo-sql/
    #[test]
    fn matching_inbox_naive() {
        assert_todo_matches("(A) in inbox", "ccontext is null", true);

        assert_todo_matches("(A) in @context", "ccontext is null", false);
    }

    #[test]
    fn matching_active_tasks_naive() {
        let e = r#"
            ccontext not in ('@@delegated', '@@blocked', '@@maybe',)
            and (
                threshold_date is null
                or threshold_date <= date('today')
            )
            "#;
        assert_todo_matches("(A) in @context", e, true);
        assert_todo_matches(
            "(A) in @@blocked", // in blocked
            e,
            false,
        );
        assert_todo_matches(
            "(A) in @home t:3000-10-10", // in future
            e,
            false,
        );
        assert_todo_matches(
            "(A) in @home t:1000-10-10", // in past
            e,
            true,
        );
    }

    #[test]
    fn matching_hidden_tasks() {
        assert_todo_matches("(A) booo h:1", "is_hidden is true", true);
        assert_todo_matches("(A) booo h:1", "is_hidden", true);
        assert_todo_matches("(A) booo h:1", "is_hidden = true", true);
        assert_todo_matches("(A) booo h:0", "is_hidden == false", true);
    }

    // array/set cases from:
    // https://vapor.visint.in/1_rings/tudor/todo-sql-sets/
    #[test]
    fn matching_sets_tasks() {
        assert_todo_matches(
            "(A) @AAA @home watch @tv",
            "not (contexts && ('@tv', '@gameboy',))",
            false,
        );
        assert_todo_matches(
            "(A) @home fix leak",
            "not (contexts && ('@tv', '@gameboy',))",
            true,
        );

        assert_todo_matches("(A) fix leak", "is_empty(contexts)", true);
    }

    #[test]
    fn overlap_for_sets_and_text() {
        assert_todo_matches("+foo ensure the bars are bazzed", "tags && '+foo'", true);
        // this is mainly useful to do the right thing when right is parenthesized
        assert_todo_matches(
            "+foo ensure the bars are bazzed",
            "tags && ('+foo')", // it'd have to be ('+foo',) to be parsed as a set
            true,
        );
    }

    #[test]
    fn using_aliases() {
        assert_todo_matches("(A) do it", "is_complete is true", false);
        assert_todo_matches("(A) do it", "done is true", false);
        assert_todo_matches(
            "(A) @AAA @home watch @tv",
            "lists && ('@tv', '@gameboy',)",
            true,
        );
        assert_todo_matches("(A) +foobar foo the bars", "tags && ('+foobar',)", true);
    }

    #[bench]
    fn parsing_active_view_expression(b: &mut Bencher) {
        let active = r#"
            ( not (contexts && ('@@delegated', '@@blocked', '@@maybe', '@ikea')))
             and (
                hidden is not true
            ) and (
                threshold_date is null
                or threshold_date <= date('today')
            )
        "#;
        b.iter(|| Expression::parse(active))
    }

    #[bench]
    fn parsing_inbox_view_expression(b: &mut Bencher) {
        let inbox = r#"
            is_empty(contexts)
        "#;
        b.iter(|| Expression::parse(inbox))
    }
}
