use std::collections::BTreeSet;

use pest_consume::Parser;

use crate::sql::expression::parser::{ExpressionParser, Result, Rule};

// TODO: investigate whether this shouldn't be `Eq` since comparing diff types could be undefined?
#[derive(Eq, Ord, PartialOrd, PartialEq, Debug, Clone)]
pub enum Value {
    Null,
    Boolean(bool),
    /// `Date` is not parsed, user creates it through `crate::sql::expression::function::Function`
    Date(time::Date),
    Text(String),
    // we only support sets of strings for now
    Set(BTreeSet<String>),
}
// TODO: something like this? and Ord too?
// impl PartialEq for Value {
//     fn eq(&self, other: &Self) -> bool {
//         match (self, other) {
//             (Value::Null, Value::Null) => false,
//             (Value::Boolean(b1), Value::Boolean(b2)) => b1 == b2,
//             (Value::Date(d1), Value::Date(d2)) => d1 == d2,
//             (Value::Text(s1), Value::Text(s2)) => s1 == s2,
//             _ => false
//         }
//     }
// }

impl Value {
    pub fn is_null(&self) -> bool {
        matches!(self, Value::Null)
    }

    pub fn is_true(&self) -> bool {
        matches!(self, Value::Boolean(true))
    }

    pub fn is_false(&self) -> bool {
        matches!(self, Value::Boolean(false))
    }

    pub fn parse(input: &str) -> Result<Value> {
        let nodes = ExpressionParser::parse(Rule::value, input)?;
        let node = nodes.single()?;
        let v = ExpressionParser::value(node)?;
        Ok(v)
    }

    pub fn set(input: &[&str]) -> Value {
        Value::Set(input.iter().map(ToString::to_string).collect())
    }

    pub fn text(input: &str) -> Value {
        Value::Text(input.to_string())
    }
}

#[cfg(test)]
mod test {

    // #[test]
    // fn value_equality() {
    //     assert_eq!(Value::Boolean(true), Value::Boolean(true));
    //     assert_ne!(Value::Boolean(true), Value::Boolean(false));
    //     assert_ne!(Value::Date(time::macros::date!(2021-01-01)), Value::Boolean(true));
    //     assert_ne!(Value::Date(time::macros::date!(2021-01-01)), Value::Date(time::macros::date!(2021-01-01)));
    //     assert_ne!(Value::Null, Value::Null);
    // }
}
