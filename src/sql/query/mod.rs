// TODO rm me
#![allow(warnings)]

use crate::sql::expression::todo_field::*;
use crate::sql::query::select::*;
use crate::todo::*;

mod select;

#[derive(Eq, PartialEq, Debug)]
struct GroupBy {
    by: TodoField,
}

#[derive(Eq, PartialEq, Debug)]
struct OrderBy {
    by: TodoField,
    desc: bool,
}

#[derive(Default, Eq, PartialEq, Debug)]
struct Query {
    select: Select,
    group_by: Option<GroupBy>,
    order_by: Option<OrderBy>,
}

impl Query {
    fn matches(&self, t: &Todo) -> bool {
        unimplemented!();
    }
}

#[cfg(test)]
mod test {

    // TODO: reimplement me
    // #[test]
    // fn matching_todos_with_empty_where() {
    //
    //     let t = Todo::parse("x (A) ma vai").unwrap();
    //
    //     let q = Query::builder().build();
    //     assert!(q.matches(&t))
    // }
}
