use crate::sql::expression::*;

#[derive(Eq, PartialEq, Debug)]
enum SelectFields {
    Star,
    // TODO: Fields(HashSet<Field>),
    // TODO: distinct
}

impl Default for SelectFields {
    fn default() -> Self {
        SelectFields::Star
    }
}

#[derive(Eq, PartialEq, Debug)]
enum SelectFrom {
    Stdin,
    TodoTxt,
    DoneTxt,
    // TODO: other files in todo dir
}

impl Default for SelectFrom {
    fn default() -> Self {
        SelectFrom::Stdin
    }
}

#[derive(Default, Eq, PartialEq, Debug)]
pub struct Select {
    fields: SelectFields,
    from: SelectFrom,
    where_: Option<Expression>,
}
