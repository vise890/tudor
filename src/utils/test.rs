#![cfg(test)]

use simplelog::{ColorChoice, Config, LevelFilter, TerminalMode, TermLogger};

pub fn init() {
    // FIXME: how can i send to stdout w/o messing up cargo's output?
    let l = TermLogger::init(
        LevelFilter::Debug,
        Config::default(),
        TerminalMode::Stdout,
        ColorChoice::Auto,
    );
    if l.is_err() {
        log::trace!("{:?}", l)
    }
}
