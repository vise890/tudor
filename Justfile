update:
  cargo update

test:
  cargo test

install:
  cargo install --path .

upgrade:
  # https://lib.rs/crates/cargo-outdated
  # cargo outdated
  # https://lib.rs/crates/cargo-upgrades
  cargo upgrades

cut: update test install

